import * as React from 'react';
import Grid from '@material-ui/core/Grid';
import {
  TextField,
} from "@material-ui/core";
import {Formik, Field, Form} from 'formik';
import Button from "@material-ui/core/Button";
import {useStyles} from "../styles";
import {LinearProgress} from "@material-ui/core";
import {
  CardCvcElement,
  CardNumberElement,
  CardExpiryElement
} from "@stripe/react-stripe-js";
import {StripeInput} from "./StripeInput";

import {useElements, useStripe} from "@stripe/react-stripe-js";
import {Token} from "@stripe/stripe-js";
import CloseIcon from "@material-ui/icons/Close";
import {TokenDisplay} from "./components/TokenDisplay";
import {ErrorMessage} from "../main/components/errors/typings";
import {stringToErrorMessage} from "../main/errors";
import {InlineErrorMessageDisplay} from "../main/components/errors/InlineErrorMessageDisplay";

const cardsLogo = [
  "amex",
  "cirrus",
  "diners",
  "dankort",
  "discover",
  "jcb",
  "maestro",
  "mastercard",
  "visa",
  "visaelectron",
];

type PaymentInfoFormProps = {
  handleBack: () => void,
  handleTokenSuccess: (token: Token) => void,
  handleTokenClear: () => void,
  token?: Token
}

export function PaymentInfoForm(props: PaymentInfoFormProps) {
  const classes = useStyles();

  const stripe = useStripe()!;
  const elements = useElements()!;

  const [errorMessage, setErrorMessage] = React.useState<ErrorMessage | undefined>();

  const onSubmit = async () => {
    const cardElement = elements.getElement(CardNumberElement)!;
    const result = await stripe.createToken(cardElement);

    if (!result.error) {
      props.handleTokenSuccess(result.token!);
    } else {
      setErrorMessage(stringToErrorMessage(result.error!.message!));
    }
  }

  const handleNext = () => {
    props.handleTokenSuccess(props.token!);
  };

  return !props.token ? (
    <React.Fragment>
      <Formik
        initialValues={{}}
        onSubmit={async (values, {setSubmitting}) => {
          await onSubmit();
        }}
      >
        {({values, isSubmitting}) => (
          <Form>
            <Grid container spacing={3}>
              <Grid container item xs={12}>
                <Grid container item xs={12} justify="space-between">
                  {cardsLogo.map(e => <img key={e} src={`./cards/${e}.png`} alt={e} width="50px"
                                           style={{padding: "0 5px"}}/>)}
                </Grid>
              </Grid>
              <Grid item xs={12} sm={6}>
                <TextField
                  label="Credit Card Number"
                  name="cardNumber"
                  fullWidth
                  InputProps={{
                    inputProps: {
                      component: CardNumberElement
                    },
                    inputComponent: StripeInput
                  }}
                  InputLabelProps={{shrink: true}}
                />
              </Grid>
              <Grid item xs={6} sm={6}>
                <Field
                  component={TextField}
                  label="Expiration Date"
                  name="cardExpiry"
                  fullWidth
                  InputProps={{
                    inputProps: {
                      component: CardExpiryElement
                    },
                    inputComponent: StripeInput
                  }}
                  InputLabelProps={{shrink: true}}
                />
              </Grid>
              <Grid item xs={6} sm={6}>
                <Field
                  component={TextField}
                  label="CVC"
                  name="cardCvc"
                  required
                  fullWidth
                  InputProps={{
                    inputProps: {
                      component: CardCvcElement
                    },
                    inputComponent: StripeInput
                  }}
                  InputLabelProps={{shrink: true}}
                />
              </Grid>
            </Grid>
            {isSubmitting && <LinearProgress/>}
            <div className={classes.inlineErrors}>
              <InlineErrorMessageDisplay errorMessage={errorMessage} />
            </div>
            <div className={classes.buttons}>
              <Button
                variant="contained"
                className={classes.button}
                onClick={props.handleBack}
              >
                Back
              </Button>
              <Button
                variant="contained"
                color="primary"
                className={classes.button}
                type="submit"
              >
                Next
              </Button>
            </div>
          </Form>
        )}
      </Formik>
    </React.Fragment>
  ) : (
    <React.Fragment>
      <Grid container spacing={2}>
        <Grid item xs={12} sm={8}>
          <TokenDisplay token={props.token}/>
          <Grid item xs={6}>
          </Grid>
          <Grid item xs={6}>
            <Button color="primary" onClick={props.handleTokenClear}>
              <CloseIcon/>
              Clear
            </Button>
          </Grid>
        </Grid>
      </Grid>
      <div className={classes.buttons}>
        <Button
          variant="contained"
          className={classes.button}
          onClick={props.handleBack}
        >
          Back
        </Button>
        <Button
          variant="contained"
          color="primary"
          className={classes.button}
          onClick={handleNext}
        >
          Next
        </Button>
      </div>
    </React.Fragment>
  );
}
