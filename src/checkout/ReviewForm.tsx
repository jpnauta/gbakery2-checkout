import * as React from 'react';
import Typography from '@material-ui/core/Typography';
import Grid from '@material-ui/core/Grid';
import Button from "@material-ui/core/Button";
import {useStyles} from "../styles";
import {Token} from "@stripe/stripe-js";
import {Order} from "../main/api/typings";
import {TokenDisplay} from "./components/TokenDisplay";
import {DateTime} from "luxon";
import {TIMEZONE} from "../main/config";
import {Form, Formik} from "formik";
import {formSubmitWrapper} from "../main/api";
import {ErrorMessage} from "../main/components/errors/typings";
import {InlineErrorMessageDisplay} from "../main/components/errors/InlineErrorMessageDisplay";
import {LinearProgress} from "@material-ui/core";

type ReviewFormProps = {
  handleBack: () => void,
  order: Order,
  token: Token,
  handleSubmit: (order: Order) => Promise<void>
}

export function ReviewForm(props: ReviewFormProps) {
  const classes = useStyles();

  // Sync token ID into order to be sent to server
  props.order.stripe_token_id = props.token.id;

  const dt = DateTime.fromFormat(`${props.order.date} ${props.order.time}`, "yyyy-MM-dd HH:mm:ss").setZone(TIMEZONE);

  const [errorMessage, setErrorMessage] = React.useState<ErrorMessage | undefined>();

  return (
    <React.Fragment>
      <Formik
        initialValues={props.order}
        onSubmit={async (values, {setSubmitting}) => {
          await formSubmitWrapper(props.handleSubmit(values), setSubmitting, setErrorMessage);
        }}
      >
        {({setFieldValue, values, isSubmitting}) => (
          <Form>
            <Grid container spacing={2}>
              <Grid item container direction="column" xs={12}>
                <Typography variant="h6" gutterBottom className={classes.title}>
                  Personal Info
                </Typography>
                <Grid container>
                  <React.Fragment>
                    <Grid item xs={5}>
                      <Typography gutterBottom>Name</Typography>
                    </Grid>
                    <Grid item xs={7}>
                      <Typography gutterBottom>{props.order.name}</Typography>
                    </Grid>
                    <Grid item xs={5}>
                      <Typography gutterBottom>Email</Typography>
                    </Grid>
                    <Grid item xs={7}>
                      <Typography gutterBottom>{props.order.email}</Typography>
                    </Grid>
                    <Grid item xs={5}>
                      <Typography gutterBottom>Phone Number</Typography>
                    </Grid>
                    <Grid item xs={7}>
                      <Typography gutterBottom>{props.order.phone_number}</Typography>
                    </Grid>
                    <Grid item xs={5}>
                      <Typography gutterBottom>Date</Typography>
                    </Grid>
                    <Grid item xs={7}>
                      <Typography gutterBottom>{dt.toLocaleString(DateTime.DATETIME_MED)}</Typography>
                    </Grid>
                  </React.Fragment>
                </Grid>
              </Grid>
            </Grid>
            <Typography variant="h6" className={classes.title}>
              Products
            </Typography>
            <Grid item container direction="column" xs={12}>
              <Grid container>
                {props.order.order_products.map((op) => (
                  <React.Fragment key={op.front_product_price}>
                    <Grid item xs={5}>
                      <Typography gutterBottom>{op.description!}</Typography>
                    </Grid>
                    <Grid item xs={7}>
                      <Typography gutterBottom>
                        ${op.total_calculated_price!} CAD
                      </Typography>
                    </Grid>
                  </React.Fragment>
                ))}
                <Grid item xs={5}>
                  <Typography gutterBottom>Total</Typography>
                </Grid>
                <Grid item xs={7}>
                  <Typography className={classes.total} gutterBottom>
                    ${props.order.calculated_price!} CAD
                  </Typography>
                </Grid>
              </Grid>
            </Grid>
            <Grid container spacing={2}>
              <Grid item container direction="column" xs={12}>
                <Typography variant="h6" gutterBottom className={classes.title}>
                  Payment Details
                </Typography>
                <TokenDisplay token={props.token}/>
              </Grid>
            </Grid>
            {isSubmitting && <LinearProgress/>}
            <div className={classes.inlineErrors}>
              <InlineErrorMessageDisplay errorMessage={errorMessage} />
            </div>
            <div className={classes.buttons}>
              <Button
                variant="contained"
                className={classes.button}
                onClick={props.handleBack}
                disabled={isSubmitting}
              >
                Back
              </Button>
              <Button
                variant="contained"
                color="primary"
                className={classes.button}
                disabled={isSubmitting}
                type="submit"
              >
                Place Order
              </Button>
            </div>
          </Form>
        )}
      </Formik>
    </React.Fragment>
  );
}
