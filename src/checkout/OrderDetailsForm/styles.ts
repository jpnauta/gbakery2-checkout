import { makeStyles } from "@material-ui/core/styles";

export const useStyles = makeStyles((theme) => ({
  borderButton: {
    border: `1px dashed ${theme.palette.secondary.main}`,
  },
  modalPaper: {
    margin: theme.spacing(2),
    [theme.breakpoints.up('md')]: {
      width: "80vw",
      height: "80vh",
      maxWidth: "80vw",
      maxHeight: "80vh",
    },
  },
  productName: {
    display: "flex",
    justifyContent: "flex-end",
    flexDirection: "column",
  },
  orderItemsContainer: {
    padding: theme.spacing(1),
  },
  orderTable: {
    minWidth: "100%"
  }
}));
