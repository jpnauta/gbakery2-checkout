import * as React from 'react';
import * as yup from "yup";
import find from 'lodash/find';
import sum from 'lodash/sum';
import map from 'lodash/map';
import Button from "@material-ui/core/Button";
import Dialog from '@material-ui/core/Dialog';
import IconButton from '@material-ui/core/IconButton';
import LinearProgress from "@material-ui/core/LinearProgress";
import MenuItem from '@material-ui/core/MenuItem';
import {HighlightOff} from '@material-ui/icons';
import {Formik, Field, Form, FieldArray} from 'formik';
import {TextField, Select} from 'formik-material-ui';
import Table from '@material-ui/core/Table';
import TableBody from '@material-ui/core/TableBody';
import TableCell from '@material-ui/core/TableCell';
import TableContainer from '@material-ui/core/TableContainer';
import TableHead from '@material-ui/core/TableHead';
import TableRow from '@material-ui/core/TableRow';


import {FormModel, required} from "../../main/forms/models";

import {ProductSelector} from '../ProductSelector';

import {useStyles} from "../../styles";
import {useStyles as useLocalStyles} from './styles';
import Typography from "@material-ui/core/Typography";
import {Box} from "@material-ui/core";
import Paper from '@material-ui/core/Paper';
import {ContactUsMessage} from "../components/ContactUsMessage";

export const OrderDetailsFieldsFormModel = new FormModel(
  yup.object({
    order_items: yup.array().of(yup.object().shape({
      name: yup.string(),
      product_price_id: yup.number().integer().min(0),
      prices: yup.array().of(yup.object().shape({
        id: yup.number().integer().min(0),
        quantity_type: yup.number().integer().min(0),
        quantity_type_name: yup.string(),
        price: yup.string()
      })),
      quantity: required(yup.number().integer().min(1)),
    })).min(1)
  }).defined(),
  () => {
    return {
      order_items: []
    }
  }
)

export type OrderDetailsFields = yup.InferType<typeof OrderDetailsFieldsFormModel.schema>;

type OrderDetailsFormProps = {
  orderDetailsFields: OrderDetailsFields,
  handleSubmit: (details: OrderDetailsFields) => void,
  categories: any,
}

export function OrderDetailsForm(props: OrderDetailsFormProps) {
  const globalClasses = useStyles();
  const localClasses = useLocalStyles();

  const [openProductDialog, setOpenProductDialog] = React.useState(false);

  const handleOpenProductDialog = () => {
    setOpenProductDialog(true);
  }

  const handleCloseProductDialog = () => {
    setOpenProductDialog(false);
  }

  // @ts-ignore
  return (
    <React.Fragment>
      <Formik
        initialValues={props.orderDetailsFields}
        onSubmit={async (values, {setSubmitting}) => {
          props.handleSubmit(values);
        }}
        validationSchema={OrderDetailsFieldsFormModel.schema}
      >
        {({setFieldValue, values, isSubmitting, isValid}) => (
          <Form>
            <ContactUsMessage/>
            <FieldArray
              name="order_items"
              render={arrayHelpers => {
                const handleProductSelect = (product: any) => {
                  setOpenProductDialog(false);
                  arrayHelpers.push({
                    ...product,
                    product_price_id: product.prices[0].id,
                    quantity: null
                  });
                }

                // Calculate total price
                let totalPrice;
                if (values?.order_items?.length) {
                  const productPrices: any[] = map(values?.order_items, (item) => {
                    const productPrice = find(item?.prices, (price) => price!.id && price!.id === item!.product_price_id);
                    return (item!.quantity && productPrice && (productPrice! as any).price * item!.quantity) || 0;
                  });
                  totalPrice = sum(productPrices);
                }

                return (
                  <React.Fragment>
                    <TableContainer component={Paper} className={localClasses.orderTable}>
                      <Table>
                        <TableHead>
                          <TableRow>
                            <TableCell>Product Name</TableCell>
                            <TableCell>Quantity</TableCell>
                            <TableCell align="right">Type</TableCell>
                            <TableCell align="right">&nbsp;</TableCell>
                          </TableRow>
                        </TableHead>
                        <TableBody>
                          {values?.order_items && values.order_items.map((item, index) => {
                              return (
                                <TableRow key={`order_items.${index}`}>
                                  <TableCell component="th" scope="row">
                                    <Typography variant="body1" gutterBottom>
                                      {item?.name}
                                    </Typography>
                                  </TableCell>
                                  <TableCell align="right">
                                    <Field
                                      component={TextField}
                                      disabled={item && item.product_price_id === null}
                                      name={`order_items.${index}.quantity`}
                                      type="number"
                                      label="Quantity"
                                      fullWidth
                                      value={item!.quantity || ""}
                                      variant="outlined"
                                    />
                                  </TableCell>
                                  <TableCell align="right">
                                    {(item?.prices?.length || 0) > 1 ? (
                                      <Field
                                        component={Select}
                                        fullWidth
                                        name={`order_items.${index}.product_price_id`}
                                        variant="outlined"
                                        label="Quantity Type">
                                        {item?.prices && item.prices.map((price) => (
                                          <MenuItem value={price?.id}>
                                            {price?.quantity_type_name} - ${price?.price}
                                          </MenuItem>
                                        ))}
                                      </Field>
                                    ) : (
                                      <React.Fragment>
                                        {item!.prices!.map((price) => (
                                          <Typography gutterBottom>
                                            <div>
                                              {price?.quantity_type_name}
                                            </div>
                                            <div>
                                              ${price?.price}
                                            </div>
                                          </Typography>
                                        ))}
                                      </React.Fragment>
                                    )}
                                  </TableCell>
                                  <TableCell align="right">
                                    <IconButton onClick={() => arrayHelpers.remove(index)}>
                                      <HighlightOff color="error"/>
                                    </IconButton>
                                  </TableCell>
                                </TableRow>
                              )
                            }
                          )}
                          {!!totalPrice && (
                            <TableRow>
                              <TableCell>
                                <Typography variant="h6" gutterBottom>
                                  Total Price
                                </Typography>
                              </TableCell>
                              <TableCell align="right" colSpan={2}>
                                <Typography variant="body1" gutterBottom>
                                  <Box fontWeight="fontWeightBold" component="span">
                                    ${Number(totalPrice).toFixed(2)}
                                  </Box>
                                </Typography>
                              </TableCell>
                              <TableCell/>
                            </TableRow>
                          )}
                          <TableRow>
                            <TableCell colSpan={4}>
                              <Button
                                onClick={handleOpenProductDialog}
                                classes={{root: localClasses.borderButton}}
                                color="secondary">
                                Click Here To Add An Item
                              </Button>
                              <Dialog
                                classes={{paper: localClasses.modalPaper}}
                                onClose={handleCloseProductDialog}
                                open={openProductDialog}>
                                <ProductSelector onClick={handleProductSelect} categories={props.categories}/>
                              </Dialog>
                            </TableCell>
                          </TableRow>
                        </TableBody>
                      </Table>
                    </TableContainer>
                  </React.Fragment>
                )
              }}/>
            {isSubmitting && <LinearProgress/>}
            <br/>
            <div className={globalClasses.buttons}>
              <Button
                variant="contained"
                color="primary"
                className={globalClasses.button}
                disabled={isSubmitting}
                type="submit"
              >
                Next
              </Button>
            </div>
          </Form>
        )}
      </Formik>
    </React.Fragment>
  );
}
