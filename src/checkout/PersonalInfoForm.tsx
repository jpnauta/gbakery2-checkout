import * as React from 'react';
import Grid from '@material-ui/core/Grid';
import {TextField} from 'formik-material-ui';
import {Formik, Field, Form} from 'formik';
import Button from "@material-ui/core/Button";
import {useStyles} from "../styles";
import {DateTimePicker} from "formik-material-ui-pickers";
import {LinearProgress} from "@material-ui/core";
import {FormModel, required} from "../main/forms/models";
import * as yup from "yup";
import {DateTime} from "luxon";
import {localNow} from "../main/ext/dates";
import {ErrorMessage} from "../main/components/errors/typings";
import {InlineErrorMessageDisplay} from "../main/components/errors/InlineErrorMessageDisplay";
import {formSubmitWrapper} from "../main/api";
import Typography from "@material-ui/core/Typography";
import {ContactUsMessage} from "./components/ContactUsMessage";

export const PersonalInfoFieldsFormModel = new FormModel(
  yup.object({
    name: required(yup.string()
      .min(1)
      .max(50)),
    email: required(yup.string()
      .email()),
    dateTime: required(yup.object<DateTime>()
      .test('min-date', 'Order must be placed at least two days in advance',
        (value) => {
          return value ? value! > localNow().startOf('day').plus({days: 2}) : false;
        })
      .test('min-hour', 'Order must be after 8AM',
        (value) => {
          return value ? value!.hour! >= 8 : false;
        })
      .test('max-hour', 'Order must be before 5PM',
        (value) => {
          return value ? value!.hour! <= 17 : false;
        })),
    phoneNumber: required(yup.string().phone("CA")),
  }).defined(),
  () => {
    return {
      name: null,
      email: null,
      phoneNumber: null,
      dateTime: localNow().startOf('day').plus({days: 2, hours: 12}),
    }
  }
)

export type PersonalInfoFields = yup.InferType<typeof PersonalInfoFieldsFormModel.schema>;

type PersonalInfoFormProps = {
  personalInfoFields: PersonalInfoFields,
  handleBack: () => void,
  handleSubmit: (personalInfoFields: PersonalInfoFields) => Promise<void>,
}

export function PersonalInfoForm(props: PersonalInfoFormProps) {
  const classes = useStyles();

  const [errorMessage, setErrorMessage] = React.useState<ErrorMessage | undefined>();

  // @ts-ignore
  return (
    <React.Fragment>
      <Formik
        initialValues={props.personalInfoFields}
        onSubmit={async (values, {setSubmitting}) => {
          await formSubmitWrapper(props.handleSubmit(values), setSubmitting, setErrorMessage);
        }}
        validationSchema={PersonalInfoFieldsFormModel.schema}
      >
        {({setFieldValue, values, isSubmitting}) => (
          <Form>
            <Grid container spacing={3}>
              <Grid container item xs={12}>
                <ContactUsMessage/>
              </Grid>
              <Grid item xs={12} sm={6}>
                <Field
                  component={TextField}
                  name="name"
                  label="Full Name"
                  fullWidth
                  autoComplete="name"
                  value={values.name || ""}
                />
              </Grid>
              <Grid item xs={12} sm={6}>
                <Field
                  component={TextField}
                  name="email"
                  label="Email"
                  fullWidth
                  autoComplete="email"
                  value={values.email || ""}
                />
              </Grid>
              <Grid item xs={12} sm={6}>
                <Field
                  component={TextField}
                  name="phoneNumber"
                  label="Phone Number"
                  fullWidth
                  autoComplete="tel-national"
                  value={values.phoneNumber || ""}
                />
              </Grid>
              <Grid item xs={12} sm={6}>
                <Field
                  component={DateTimePicker}
                  name="dateTime"
                  label="Pickup Date/Time"
                  fullWidth
                  value={values.dateTime}
                  // For some reason this component requires both `value` and `onChange`...
                  onChange={(option: DateTime) => {
                    setFieldValue("dateTime", option);
                  }}
                />
              </Grid>
              <Grid item xs={12}>
                <Typography variant="body2" className={classes.warningText}>
                  Due to large numbers of orders, please pick an appropriate hour to come
                  so that we can spread out the customers!
                </Typography>
              </Grid>
            </Grid>
            {isSubmitting && <LinearProgress/>}
            <div className={classes.inlineErrors}>
              <InlineErrorMessageDisplay errorMessage={errorMessage} />
            </div>
            <div>

              <div className={classes.buttons}>
                <Button
                  variant="contained"
                  className={classes.button}
                  onClick={props.handleBack}
                >
                  Back
                </Button>
                <Button
                  variant="contained"
                  color="primary"
                  className={classes.button}
                  disabled={isSubmitting}
                  type="submit"
                >
                  Next
                </Button>
              </div>
            </div>
          </Form>
        )}
      </Formik>
    </React.Fragment>
  );
}
