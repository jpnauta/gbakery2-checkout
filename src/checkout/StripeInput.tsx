import React, {useRef, useImperativeHandle} from 'react'
import {InputBaseComponentProps} from "@material-ui/core/InputBase/InputBase";

// TODO docs
// @ts-ignore
export const StripeInput: React.ElementType<InputBaseComponentProps> = ({component: Component, inputRef, ...other}) => {
  const elementRef = useRef();
  useImperativeHandle(inputRef, () => ({
    // @ts-ignore
    focus: () => elementRef.current.focus
  }));

  return (
    // @ts-ignore
    <Component onReady={element => (elementRef.current = element)} {...other} />
  );
}
