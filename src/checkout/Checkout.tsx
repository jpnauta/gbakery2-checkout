import * as React from 'react';
import CssBaseline from '@material-ui/core/CssBaseline';
import AppBar from '@material-ui/core/AppBar';
import Toolbar from '@material-ui/core/Toolbar';
import Paper from '@material-ui/core/Paper';
import Stepper from '@material-ui/core/Stepper';
import Step from '@material-ui/core/Step';
import StepLabel from '@material-ui/core/StepLabel';
import Link from '@material-ui/core/Link';
import Typography from '@material-ui/core/Typography';
import {PersonalInfoFields, PersonalInfoFieldsFormModel, PersonalInfoForm} from './PersonalInfoForm';
import {ReviewForm} from './ReviewForm';
import {useStyles} from "../styles";
import {PaymentInfoForm} from "./PaymentInfoForm";
import {useEffect} from "react";
import {LargeLoadingIndicator} from "../main/components/loading/LargeLoadingIndicator";
import {ErrorMessage} from "../main/components/errors/typings";
import {LargeErrorMessageDisplay} from "../main/components/errors/LargeErrorMessageDisplay";
import {createOrder, getAllProductCategories, loadDataWrapper, validateOrder} from "../main/api";
import {OrderDetailsFields, OrderDetailsFieldsFormModel, OrderDetailsForm} from "./OrderDetailsForm";
import {Token} from "@stripe/stripe-js";
import {Order} from "../main/api/typings";
import {Box, MobileStepper, withWidth} from "@material-ui/core";
import {asyncWrapper} from "../main/ext/functions";

function Copyright() {
  return (
    <Typography variant="body2" color="textSecondary" align="center">
      {'© '}
      <Link color="inherit" href="https://glamorganbakery.com/">
        Glamorgan Bakery
      </Link>{' '}
      {new Date().getFullYear()}
    </Typography>
  );
}

const steps = ['Order Info', 'Personal Info', 'Payment Info', 'Confirm Order'];

const buildOrder = (personalInfoFields: PersonalInfoFields,
                    orderDetailsFields: OrderDetailsFields,
                    token?: Token): Order => {
  return {
    name: personalInfoFields.name!,
    email: personalInfoFields.email!,
    phone_number: personalInfoFields.phoneNumber!,
    date: personalInfoFields.dateTime!.toFormat('yyyy-MM-dd'),
    time: personalInfoFields.dateTime!.toFormat('HH:mm:ss'),
    stripe_token_id: token?.id,
    order_products: orderDetailsFields.order_items!.map((orderItem: any) => {
      return {
        front_product_price: orderItem!.product_price_id!,
        quantity: orderItem!.quantity!,
      };
    }),
  }
}

export function Checkout(props: any) {
  const mobile = 'xs' === props.width;
  const classes = useStyles();
  const [activeStep, setActiveStep] = React.useState(0);
  // eslint-disable-next-line @typescript-eslint/no-unused-vars
  const [orderDetailsFields, setOrderDetailsFields] = React.useState<OrderDetailsFields>(OrderDetailsFieldsFormModel.build());

  const [personalInfoFields, setPersonalInfoFields] = React.useState<PersonalInfoFields>(PersonalInfoFieldsFormModel.build());

  // eslint-disable-next-line @typescript-eslint/no-unused-vars
  const [order, setOrder] = React.useState<Order | undefined>();

  const [token, setToken] = React.useState<Token | undefined>();

  const [categories, setCategories] = React.useState<any>();

  const [errorMessage, setErrorMessage] = React.useState<ErrorMessage | undefined>();

  // Load category info on startup
  useEffect(() => {
    asyncWrapper(async () => {
      await loadDataWrapper(getAllProductCategories(), setCategories, setErrorMessage);
    });
  }, []);

  const nextStep = () => {
    setActiveStep(activeStep + 1);
  };

  const handleOrderInfoComplete = (orderDetailsFields: OrderDetailsFields) => {
    setOrderDetailsFields(orderDetailsFields);
    nextStep();
  };

  const handlePersonalInfoComplete = async (personalInfoFields: PersonalInfoFields) => {
    setPersonalInfoFields(personalInfoFields);

    // Validate order with server
    let order = buildOrder(personalInfoFields, orderDetailsFields);
    order = await validateOrder(order);

    setOrder(order);
    nextStep();
  };

  const handleTokenSuccess = (token: Token) => {
    setToken(token);
    nextStep();
  };

  const handleTokenClear = () => {
    setToken(undefined);
  };

  const handleBack = () => {
    setActiveStep(activeStep - 1);
  };

  const handlePlaceOrder = async (order: Order) => {
    // Validate order with server
    order = await createOrder(order);
    setOrder(order);
    nextStep();
  };

  const getStepContent = (step: number) => {
    switch (step) {
      case 0:
        return <OrderDetailsForm orderDetailsFields={orderDetailsFields} handleSubmit={handleOrderInfoComplete}
                              categories={categories}/>;
      case 1:
        return <PersonalInfoForm personalInfoFields={personalInfoFields} handleBack={handleBack}
                                 handleSubmit={handlePersonalInfoComplete}/>;
      case 2:
        return <PaymentInfoForm handleBack={handleBack} handleTokenSuccess={handleTokenSuccess}
                                handleTokenClear={handleTokenClear} token={token}/>;
      case 3:
        return <ReviewForm handleBack={handleBack} order={order!} token={token!} handleSubmit={handlePlaceOrder}/>;
      default:
        throw new Error('Unknown step');
    }
  };

  return (
    <div className={classes.root}>
      <CssBaseline/>
      <AppBar position="absolute" color="transparent" className={classes.appBar}>
        <Toolbar>
          <Link href="https://glamorganbakery.com/">
            <img src="./logo-original.png" alt="logo" className={classes.appBarLogo}/>
          </Link>
        </Toolbar>
      </AppBar>
      <main className={classes.layout}>
        <LargeErrorMessageDisplay errorMessage={errorMessage}>
          <LargeLoadingIndicator isLoading={!categories}>
            <Paper className={classes.paper}>
              <Typography component="h1" variant="h5" align="center">
                Checkout
              </Typography>
              { mobile 
                ? (
                  <MobileStepper
                    classes={{ root: classes.mobileStepper }}
                    variant="progress"
                    steps={6}
                    position="static"
                    nextButton={<div />}
                    backButton={<div />}
                    activeStep={activeStep}/>
                ) 
                : (
                <Stepper activeStep={activeStep} className={classes.stepper}>
                {steps.map((label) => (
                  <Step key={label}>
                    <StepLabel>{label}</StepLabel>
                  </Step>
                ))}
              </Stepper>)}
              <React.Fragment>
                {activeStep === steps.length ? (
                  <React.Fragment>
                    <Typography variant="h5" gutterBottom>
                      Thank you for your order!
                    </Typography>
                    <Typography gutterBottom>
                      Your order number is #{order!.id}. We have emailed an order
                      confirmation to {order!.email}.
                    </Typography>
                    <Typography gutterBottom>
                      <Box fontWeight="fontWeightBold" component="span">
                        If you have any questions, or would like to change/cancel your order, please respond via email!
                      </Box>
                      {" "}
                      We will contact you in the next 24 hours if there are any issues with your order.
                    </Typography>
                  </React.Fragment>
                ) : (
                  <React.Fragment>
                    {getStepContent(activeStep)}
                  </React.Fragment>
                )}
              </React.Fragment>
            </Paper>
          </LargeLoadingIndicator>
        </LargeErrorMessageDisplay>
        <Copyright/>
      </main>
    </div>
  );
}

export default withWidth()(Checkout);