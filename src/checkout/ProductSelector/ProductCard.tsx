import React from 'react';
import Card from '@material-ui/core/Card';
import CardActionArea from '@material-ui/core/CardActionArea';
import CardContent from '@material-ui/core/CardContent';
import CardMedia from '@material-ui/core/CardMedia';
import Typography from '@material-ui/core/Typography';

import { useStyles } from './styles';

interface ProductCardPropTypes {
  product?: any;
  onClick?: Function,
}

export const ProductCard = ({ product, onClick }: ProductCardPropTypes) => {
  const classes = useStyles();

  const handleClick = () => onClick && onClick(product);;

  return (
    <Card classes={{ root: classes.productCard }}>
      <CardActionArea onClick={handleClick}>
        <CardMedia
          className={classes.productMedia}
          image={product.img_med}
          title={product.name}
        />
        <CardContent>
          <Typography gutterBottom variant="h5" component="h2">
            {product.name}
          </Typography>
        </CardContent>
      </CardActionArea>
    </Card>
  );
}