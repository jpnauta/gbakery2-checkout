import React from 'react';
import {ProductCard} from './ProductCard';
import { Grid } from '@material-ui/core';
import { useStyles } from './styles';

interface TabPanelProps {
  category: any;
  onClick?: Function,
}

export const TabPanel = (props: TabPanelProps) => {
  const { category, onClick, ...other } = props;
  
  const classes = useStyles();

  const handleClick = (product: any) => onClick && onClick(product);

  const products = category.front_products.sort((a: any, b: any) => Number(b.rating) - Number(a.rating));

  return (
    <div
      className={classes.productPanel}
      role="tabpanel"
      {...other}
    >
      <Grid spacing={2} container>
        {products.map((product: any) => (
          <Grid key={product.name} item lg={4} md={6} xs={12}>
            <ProductCard onClick={handleClick} product={product} />
          </Grid>)
        )}
      </Grid>
    </div>
  );
}