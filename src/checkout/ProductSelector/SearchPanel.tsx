import clsx from 'clsx';
import React from 'react';
import flatten from 'lodash/flatten';
import map from 'lodash/map';
import Grid from '@material-ui/core/Grid';

import {ProductCard} from './ProductCard';
import { useStyles } from './styles';
interface SearchPanelProps {
  categories?: Array<any>;
  search: string;
  onClick?: Function,
}

export const SearchPanel = ({ search, categories, onClick, ...other }: SearchPanelProps) => {
  const classes = useStyles();

  const products = flatten(
    map(categories, 'front_products')
  ).filter(x => {
    return x?.name && x.name.toUpperCase().includes(search.toUpperCase())
  });

  const handleClick = (product: any) => onClick && onClick(product);

  return (
    <div
      className={clsx(classes.productPanel, search && classes.withSearch)}
      role="tabpanel"
      {...other}
    >
      <Grid spacing={2} container>
        {products.map((product: any) => (
          <Grid key={product.name} item lg={4} md={6} xs={12}>
            <ProductCard onClick={handleClick} product={product} />
          </Grid>)
        )}
      </Grid>
    </div>
  );
}