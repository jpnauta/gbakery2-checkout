import { makeStyles } from "@material-ui/core";

export const useStyles = makeStyles((theme) => ({
  categoryText: {
    paddingLeft: theme.spacing(3),
    color: 'rgba(255, 255, 255, 0.7)',
  },
  root: {
    flexGrow: 1,
    width: "100%",
    backgroundColor: theme.palette.background.paper,
  },
  productMedia: {
    height: 140,
  },
  productCard: {
    background: theme.palette.action.hover,
  },
  productPanel: {
    height: "calc(90vh - 285px)",
    overflow: "auto",
    padding: theme.spacing(2),

    "&$withSearch": {
      height: "calc(90vh - 75px)",
    },
  },
  withSearch: {},
  searchInput: {
    margin: theme.spacing(1),
  },
  searchRoot: {
    margin: theme.spacing(3),
    marginBottom: 0,
  },
  seperator: {
    color: theme.palette.primary.main,
    display: "flex",
    justifyContent: "center",
    padding: theme.spacing(2),

    "&:after": {
      content: '""',
      width: "100%",
      borderTop: `1px solid ${theme.palette.primary.main}`,
      height: 1,
      margin: "auto",
    },
    "&:before": {
      content: '""',
      width: "100%",
      borderTop: `1px solid ${theme.palette.primary.main}`,
      height: 1,
      margin: "auto",
    },
  },
  seperatorText: {
    margin: 5,
  },
}));
