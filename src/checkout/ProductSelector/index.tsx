import * as React from 'react';
import uniq from 'lodash/uniq';
import map from 'lodash/map';
import debounce from 'lodash/debounce';
import Grid from '@material-ui/core/Grid';
import Input from '@material-ui/core/Input';
import Tab from '@material-ui/core/Tab';
import Tabs from '@material-ui/core/Tabs';

import { useStyles } from './styles';
import { SearchPanel } from './SearchPanel';
import { TabPanel } from './TabPanel';
import { FormControl, InputAdornment, InputLabel, Typography } from '@material-ui/core';
import { Search } from '@material-ui/icons';

type ProductSelectorProps = {
  categories: any;
  onClick?: Function;
}

export function ProductSelector(props: ProductSelectorProps) {
  const classes = useStyles()

  const { onClick } = props;

  const categories = props.categories
    .map((category: any) => {
      const front_products = category.front_products.filter((product: any) => {
        return product.prices.length > 0
      });
      return Object.assign({}, { ...category, front_products })
    })
    .filter((category: any) => category.front_products.length > 0)
    .map((category: any) => {
      const average_rating: number = category.front_products.length > 0 
        ? category.front_products.reduce((sum: number, b: any) => {
          const val = Number(sum) + Number(b.rating);
          return val;
        }, 0)
        : 0;
      
      const front_products = category.front_products.map((product: any) => ({
        ...product,
        category_id: category.id,
        category_name: category.name
      }));

      return Object.assign(category, { average_rating, front_products });
    });

  const tabOptions = uniq(map(categories.sort((a: any, b: any) => b.average_rating - a.average_rating), 'name'));

  const [selectedTab, setSelectedTab] = React.useState(0);

  const handleTabSelect = (_: any, newValue: number) => {
    setSelectedTab(newValue)
  }

  const handleClick = (product: any) => onClick && onClick(product);

  const [search, setSearch] = React.useState<string>();
  const handleSearch = debounce((event: React.ChangeEvent<HTMLInputElement>) => {
    setSearch(event.target.value)
  }, 200);



  // @ts-ignore
  return (
    <div className={classes.root}>
      <Grid container>
        <Grid classes={{ root: classes.searchRoot }} item xs={12}>
          <FormControl fullWidth>
            <InputLabel htmlFor="search-products">Search Keywords</InputLabel>
            <Input 
              id="search-products" 
              startAdornment={
                <InputAdornment position="start">
                  <Search />
                </InputAdornment>
              }
              onChange={handleSearch} />
          </FormControl>
        </Grid>
      </Grid>
      { search ? (
        <SearchPanel search={search} categories={categories} onClick={handleClick} />
      ) : (
        <React.Fragment>
          <div className={classes.seperator}>
            <Typography className={classes.seperatorText} color="primary">OR</Typography>
          </div>
          <Typography className={classes.categoryText} variant="caption">Search Categories</Typography>
          <Tabs 
            value={selectedTab} 
            onChange={handleTabSelect} 
            scrollButtons="on" 
            indicatorColor="primary" 
            textColor="primary" 
            variant="scrollable">
            {tabOptions.map((category: any, i: number) => (
              <Tab key={category} value={i} label={category} />
            ))}
          </Tabs>
          <TabPanel onClick={handleClick} category={categories[selectedTab]} />
        </React.Fragment>
      )}
    </div>
  );
}
