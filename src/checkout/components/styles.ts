import { makeStyles } from "@material-ui/core/styles";

export const useStyles = makeStyles((theme) => ({
  contactUsInfo: {
    textAlign: "center",
    marginBottom: "6vh",
    width: "100%"
  },
}));
