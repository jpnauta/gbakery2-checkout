import Typography from "@material-ui/core/Typography";
import Link from "@material-ui/core/Link";
import * as React from "react";
import {useStyles} from "./styles";


export const ContactUsMessage = () => {
  const classes = useStyles();

  return (
    <div className={classes.contactUsInfo}>
      <p>
        <Typography variant="body2" color="textSecondary">
          Can't find what you're looking for? Do you have questions before placing your order?
          Please
          {" "}
          <Link href="https://glamorganbakery.com/contact-us" target="_blank">
            contact us
          </Link>
          {" "}
          instead!
        </Typography>
      </p>
    </div>
  );
}
