import Typography from "@material-ui/core/Typography";
import Grid from "@material-ui/core/Grid";
import * as React from "react";
import {Token} from "@stripe/stripe-js";

type TokenDisplayProps = {
  token: Token
}

export const TokenDisplay = (props: TokenDisplayProps) => (
  <Grid container>
    <React.Fragment>
      <Grid item xs={5}>
        <Typography gutterBottom>Card type</Typography>
      </Grid>
      <Grid item xs={7}>
        <Typography gutterBottom>{props.token.card!.brand}</Typography>
      </Grid>
      <Grid item xs={5}>
        <Typography gutterBottom>Card number</Typography>
      </Grid>
      <Grid item xs={7}>
        <Typography gutterBottom>xxxx-xxxx-xxxx-{props.token.card!.last4}</Typography>
      </Grid>
      <Grid item xs={5}>
        <Typography gutterBottom>Expiry date</Typography>
      </Grid>
      <Grid item xs={7}>
        <Typography gutterBottom>{props.token.card!.exp_month}/{props.token.card!.exp_year}</Typography>
      </Grid>
    </React.Fragment>
  </Grid>
);
