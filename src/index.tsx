import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';
import {Elements} from "@stripe/react-stripe-js";
import {stripePromise} from "./main/stripe";
import {MuiPickersUtilsProvider} from '@material-ui/pickers';
import browserUpdate from "browser-update";
import {App} from "./App";
import {CustomLuxonUtils} from "./main/ext/dates";

// https://www.npmjs.com/package/browser-update
browserUpdate({
  i: 1000,
  text_for_i: {'msg': 'Internet Explorer is not supported on this site.'},
  reminder: 0
});

ReactDOM.render(
  <MuiPickersUtilsProvider utils={CustomLuxonUtils}>
    <Elements stripe={stripePromise}>
      <App/>
    </Elements>
  </MuiPickersUtilsProvider>,
  document.getElementById('root')
);
