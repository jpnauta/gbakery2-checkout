import {ErrorMessage} from "./components/errors/typings";
import {AxiosError} from "axios";


const axiosErrorMap = {
  500: "The server has encountered an unknown error and is unable to complete the request.",
  404: "The resource or webpage requested could be found. Please try again later!",
  0: "Could not connect to application. Please try again later!",
};

const defaultErrorMessageTitle = "An error has occurred!";

export const errorToErrorMessage = (e: any): ErrorMessage => {
  let errorMessage = {
    title: defaultErrorMessageTitle,
    detail: ""
  }

  if (e) {
    if (e.isAxiosError) {
      const error = e as AxiosError;

      if (error.request.status === 400) {
          errorMessage.detail = error.response!.data.detail;
      }

      // @ts-ignore
      const apiError = axiosErrorMap[error.request.status];
      if (apiError) {
        errorMessage.detail = apiError;
      }
    }
  }

  if (!errorMessage.detail) {
    console.error("Unhandled exception:", e);
    errorMessage.detail = "An unexpected error occurred, please try again later!";
  }

  return errorMessage;
}

/**
 * Converts a string message into an error message
 */
export const stringToErrorMessage = (s: string): ErrorMessage => {
  return {
    title: defaultErrorMessageTitle,
    detail: s
  };
}
