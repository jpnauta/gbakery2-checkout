import {createMuiTheme} from "@material-ui/core/styles";

/**
 * Creates the main theme for the application
 * @param prefersDarkMode
 */
export const createTheme = (prefersDarkMode: boolean) => {
  return createMuiTheme({
    palette: {
      // Optionally enable dark mode
      type: prefersDarkMode ? 'dark' : 'light',
      primary: {
        main: "#2D7DD2"
      },
      secondary: {
        main: "#59C3C3"
      },
      warning: {
        main: "#F59F00"
      },
      error: {
        main: "#E94F37"
      }
    },
  })
}
