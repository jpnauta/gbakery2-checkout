import * as React from 'react';
import CircularProgress from "@material-ui/core/CircularProgress";
import {useStyles} from "./styles";


type LoadingIndicatorProps = {
  isLoading: Boolean,
  children: React.ReactElement
}

/**
 * Displays a large full-screen loading indicator is `isLoading` is true, otherwise
 * displays the wrapped elements
 */
export const LargeLoadingIndicator = (props: LoadingIndicatorProps) => {
  const classes = useStyles();

  return props.isLoading ? (
    <div className={classes.root}>
      <CircularProgress size={80}/>
    </div>
  ) : props.children
};


