import {makeStyles} from "@material-ui/core/styles";


export const useStyles = makeStyles((theme) => ({
  root: {
    margin: "5em auto",
    textAlign: "center"
  },
}));
