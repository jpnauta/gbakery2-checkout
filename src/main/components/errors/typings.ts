export type ErrorMessage = {
  title: string,
  detail: string
};
