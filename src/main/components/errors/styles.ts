import {makeStyles} from "@material-ui/core/styles";
import red from "@material-ui/core/colors/red";

export const useStyles = makeStyles((theme) => ({
  largeDisplay: {
    margin: "2em",
    "font-size": "1em",
  },
  largeIcon: {
    "width": "4em",
    "height": "4em",
  },
  errorText: {
    color: red[500]
  }
}));
