import * as React from "react";
import Grid from '@material-ui/core/Grid';
import ErrorOutlineIcon from '@material-ui/icons/ErrorOutline';
import Typography from '@material-ui/core/Typography';
import {ErrorMessage} from "./typings";
import {useStyles} from "./styles";

export type LargeErrorMessageDisplayProps = {
  errorMessage: ErrorMessage | undefined,
  children: React.ReactElement
};

/**
 * Displays a large full-screen error message if the error message is given, otherwise
 * displays the wrapped elements
 */
export const LargeErrorMessageDisplay = (props: LargeErrorMessageDisplayProps) => {
  const classes = useStyles();

  return props.errorMessage ? (
    <Grid container className={classes.largeDisplay} spacing={2}>
      <Grid item xs={4}>
        <ErrorOutlineIcon className={classes.largeIcon}/>
      </Grid>
      <Grid item xs={8}>
        <Typography variant="h5" gutterBottom>
          {props.errorMessage.title}
        </Typography>
        <Typography variant="body2" gutterBottom>
          {props.errorMessage.detail}
        </Typography>
      </Grid>
    </Grid>
  ) : props.children
};
