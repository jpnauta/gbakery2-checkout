import Typography from "@material-ui/core/Typography";
import * as React from "react";
import {ErrorMessage} from "./typings";
import {useStyles} from "./styles";

export type InlineErrorMessageDisplayProps = {
  errorMessage: ErrorMessage | undefined,
};

/**
 * Displays a message inline, if given
 */
export const InlineErrorMessageDisplay = (props: InlineErrorMessageDisplayProps) => {
  const classes = useStyles();

  return (
    <React.Fragment>
      {props.errorMessage && <Typography variant="body2" className={classes.errorText}>
        {props.errorMessage.detail}
      </Typography>}
    </React.Fragment>
  );
};
