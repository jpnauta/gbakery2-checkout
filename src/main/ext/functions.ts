/**
 * Creates a wrapper around an async function
 * @param fn async function to trigger
 */
export const asyncWrapper = <T>(fn: () => Promise<T>) => {
  (async () => {
    return fn();
  })();
}
