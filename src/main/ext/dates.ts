import {DateTime} from "luxon";
import {TIMEZONE} from "../config";
import {Info} from 'luxon';
import LuxonUtils from '@date-io/luxon';


/**
 * @return current time in the local timezone
 */
export const localNow = (): DateTime => {
  return DateTime.local().setZone(TIMEZONE);
}

// https://github.com/mui-org/material-ui-pickers/issues/1270
export class CustomLuxonUtils extends LuxonUtils {
  getWeekdays() {
    // need to copy the existing, and use Info to preserve localization
    const days = [...Info.weekdaysFormat('narrow', this.locale as any)];
    // remove Sun from end of list and move to start of list
    days.unshift(days.pop() as any);
    return days;
  }

  getWeekArray(date: DateTime) {
    const { days } = date
      .endOf("month")
      .endOf("week")
      .diff(date.startOf("month").startOf("week"), "days")
      .toObject();

    let weeks: any[] = [];
    new Array(Math.round(days as any))
      .fill(0)
      .map((_, i) => i)
      .map(day =>
        date
          .startOf("month")
          .startOf("week")
          .minus({ days: 1 })
          .plus({ days: day })
      )
      .forEach((v, i) => {
        if (i === 0 || (i % 7 === 0 && i > 6)) {
          weeks.push([v]);
          return;
        }

        weeks[weeks.length - 1].push(v);
      });

    weeks = weeks.filter(week => {
      // do not allow weeks with start or end outside of current month
      return (
        week[0].hasSame(date, "month") ||
        week[week.length - 1].hasSame(date, "month")
      );
    });

    return weeks;
  }
}
