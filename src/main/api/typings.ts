export type OrderProduct = {
  front_product_price: number,
  quantity: number,

  // Sent from server
  total_calculated_price?: string,
  description?: string
}

export type Order = {
  name: string,
  email: string,
  phone_number: string,
  date: string,
  time: string,
  stripe_token_id?: string,
  order_products: OrderProduct[],

  // Sent from server
  calculated_price?: string
  id?: number  // Sent if order is complete
};
