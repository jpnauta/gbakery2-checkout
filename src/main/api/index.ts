import axios from "axios";
import {API_BASE_URL} from "../config";
import {Dispatch} from "react";
import {ErrorMessage} from "../components/errors/typings";
import {errorToErrorMessage} from "../errors";
import {Order} from "./typings";

/**
 * @return all product categories and their corresponding products and prices
 */
export const getAllProductCategories = async () => {
  const response = await axios.get(`${API_BASE_URL}public/products/categories/`) as any;

  return response.data;
};


/**
 * Creates a basic async wrapper around an API request
 *
 * @param responseFetch API request
 * @param setResponse dispatcher when response is received
 * @param setErrorMessage dispatcher when error occurs
 */
export const loadDataWrapper = async <R>(responseFetch: Promise<R>, setResponse: Dispatch<R>, setErrorMessage: Dispatch<ErrorMessage | undefined>) => {
  setErrorMessage(undefined);
  // Create async wrapper
  try {
    const response = await responseFetch;
    setResponse(response);
  } catch (e) {
    const errorMessage = errorToErrorMessage(e);
    setErrorMessage(errorMessage);
    throw e;
  }
}

/**
 * Wraps a form submit and handles logic for submitting and errors
 * @param responseFetch API request
 * @param setSubmitting function to call to indicate if submitting
 * @param setErrorMessage function to call to indicate if error occurred
 */
export const formSubmitWrapper = async <R>(responseFetch: Promise<R>, setSubmitting: Dispatch<boolean>, setErrorMessage: Dispatch<ErrorMessage | undefined>) => {
  setSubmitting(true);
  setErrorMessage(undefined);
  try {
    return await responseFetch;
  } catch (e) {
    setErrorMessage(errorToErrorMessage(e));
  } finally {
    setSubmitting(false);
  }
}

/**
 * Calls the validate order API
 * @param order validated order
 */
export const validateOrder = async (order: Order): Promise<Order> => {
  const response = await axios.post(`${API_BASE_URL}public/orders/validate/`, order);
  return response.data as Order;
};

/**
 * Calls the create order API
 * @param order created order
 */
export const createOrder = async (order: Order): Promise<Order> => {
  const response = await axios.post(`${API_BASE_URL}public/orders/`, order);
  return response.data as Order;
};
