import env from "env-var";

// URL to gbakery2 API (e.g. https://domain.com/api/)
export const API_BASE_URL = env.get("REACT_APP_API_BASE_URL")
  .required()
  .asString();

export const STRIPE_PUBLISHABLE_KEY = env.get("REACT_APP_STRIPE_PUBLISHABLE_KEY")
  .default('pk_test_51HbAtyFJw834zxH6Px227YPwkTfb55FLQsK6iZx3PrGCaAI2JN72Exjp8QfQlaG4qp2hQIDT9AF32vCZzqcQOsGL00eTBNs7nz')
  .asString();

export const TIMEZONE = 'America/Edmonton';
