import {ObjectSchema, Schema} from "yup";

// Import phone validators
import "../ext/yup/phone-numbers";

export class FormModel<T extends object> {
  schema: ObjectSchema<T>;
  build: () => T;

  constructor(schema: ObjectSchema<T>, buildFunc: () => T) {
    this.schema = schema
    this.build = buildFunc;
  }
}

/**
 * Acts the same as yup's `schema.required()`, but tricks typescript
 * into thinking that nulls are allowed, making form building easier.
 * @param schema yup schema
 */
export const required = <T>(schema: Schema<T>): Schema<T | null> => {
  return (schema as any).test('required', 'This field is required',
    (value: any) => {
      return value !== undefined && value !== null && value !== "";
    })
    .nullable()
}
