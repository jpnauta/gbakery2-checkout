declare module 'browser-update';

type Fn = () => void;

module.exports = Fn;
