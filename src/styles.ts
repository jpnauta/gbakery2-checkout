import {makeStyles} from "@material-ui/core/styles";
import red from "@material-ui/core/colors/red";


export const useStyles = makeStyles((theme) => ({
  root: {
    minWidth: 600
  },
  appBar: {
    position: 'relative'
  },
  appBarLogo: {
    // Don't know why, but there's 5px extra on the margin bottom
    maxHeight: 64 + 5,
    marginBottom: -5,
  },
  layout: {
    width: 'auto',
    marginLeft: theme.spacing(1),
    marginRight: theme.spacing(1),
    [theme.breakpoints.up('md')]: {
      width: 600,
      marginLeft: 'auto',
      marginRight: 'auto',
    },
  },
  paper: {
    marginTop: theme.spacing(3),
    marginBottom: theme.spacing(3),
    padding: theme.spacing(1),
    [theme.breakpoints.up('md')]: {
      marginTop: theme.spacing(6),
      marginBottom: theme.spacing(6),
      padding: theme.spacing(3),
    },
  },
  stepper: {
    padding: theme.spacing(3, 0, 5),
  },
  mobileStepper: {
    background: 'unset !important',
    margin: theme.spacing(2),
  },
  buttons: {
    display: 'flex',
    justifyContent: 'flex-end',
  },
  button: {
    marginTop: theme.spacing(0),
    marginLeft: theme.spacing(1),
  },
  listItem: {
    padding: theme.spacing(0, 0),
  },
  total: {
    fontWeight: 700,
  },
  title: {
    marginTop: theme.spacing(2),
  },
  inlineErrors: {
    paddingTop: theme.spacing(2),
    paddingBottom: theme.spacing(2),
    paddingLeft: theme.spacing(2),
  },
  largeIcon: {
    "width": "4em",
    "height": "4em",
  },
  errorText: {
    color: red[500]
  },
  orderImage: {
    width: 60
  },
  totalPrice: {
    padding: 10
  },
  warningText: {
    color: theme.palette.warning.main
  }
}));
